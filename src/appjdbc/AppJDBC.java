/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appjdbc;

import appjdbc.model.DBUsuario;
import appjdbc.model.Dados;
import java.io.IOException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.util.Scanner;

/**
 *
 * @author aluno
 */
public class AppJDBC extends Application {
    
    private static Stage st;
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        this.st = stage;
    }

   
    public static void main(String[] args) {
        Dados.setUsername("usuario");
        Dados.setPassword("senha");
        try{
            DBUsuario.inicializar();
            launch(args);
        } catch( Exception e ){
            System.out.println("Não deu para inicializar o DB. " + e);
            Platform.exit();
        }
    }
    
    public static void trocaTela(String str){
        try{
            Parent p = FXMLLoader.load(AppJDBC.class.getResource(str));
            st.setScene(new Scene(p));
        } catch( IOException e ){
            System.out.println("Não abriu a tela " + str);
            
        }
    }
}

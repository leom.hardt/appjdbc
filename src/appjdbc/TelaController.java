/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appjdbc;

import appjdbc.model.DBUsuario;
import appjdbc.model.Usuario;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author aluno
 */
public class TelaController implements Initializable {
    
    @FXML
    private Label mensagemSistema;
    
    @FXML
    private TextField nomeCompleto, usuario, email, senha, reSenha;
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void onAction(){
        Usuario u = new Usuario();
        
        u.setName(nomeCompleto.getText());
        u.setUsername(usuario.getText());
        u.setEmail(email.getText());
        u.setPassword(senha.getText());
        if(senha.getText().equals(reSenha.getText())){
            DBUsuario.adicionarUsuario(u);
        } else {
            mensagemSistema.setText("Senha não confere!");
        }
                
    }
    
}

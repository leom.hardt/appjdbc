/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appjdbc.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;

/**
 *
 * @author aluno
 */
public class DBUsuario {
    
    public static void inicializar() throws Exception{
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE", Dados.getUsername(), Dados.getPassword()
        );
        
        PreparedStatement ps = conn.prepareStatement("CREATE TABLE Usuarios(nome varchar2(40), "
                + "username varchar2(40) primary key not null,"
                + "password varchar2(40))");
        try{
            if(ps.execute()){
                throw new Exception("Não criou os dados");
            }
        }catch(SQLSyntaxErrorException e){
            // Se não for um erro por criar uma tabela existente
            if(! e.getLocalizedMessage().contains("ORA-00955")){
                throw e;
            }
        }catch(ClassNotFoundException e){
            System.out.println("Não carregou a classe...");
        }
    }
    
    public static void adicionarUsuario(Usuario u){
        try{
            inicializar();
            Connection conn = DriverManager.getConnection(
                    "jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE", Dados.getUsername(), Dados.getPassword()
            );
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO Usuarios(nome, username, password) values (?,?,?)");
            ps.setString(1, u.getName());
            ps.setString(2, u.getUsername());
            ps.setString(3, u.getPassword());
            
            int i = ps.executeUpdate();
            System.out.println(i);
        } catch(SQLException e){
            Dialog<String> d = new Dialog<>();
            d.getDialogPane().setContent(new Label("Usuário não adicionado. "  + e));
            d.getDialogPane().getButtonTypes().add(ButtonType.OK);
            d.showAndWait();
        } catch(Exception e){
            System.out.println("Exceção. " + e);
        }
    }
    
    public static boolean testLogin(){
        try{
            Connection conn = DriverManager.getConnection(
                    "jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE", Dados.getUsername(), Dados.getPassword()
            );
            return true;
        } catch(SQLException e){
            return false;
        }
        
    }
    
}

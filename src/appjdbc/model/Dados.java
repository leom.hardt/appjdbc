/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appjdbc.model;

/**
 *
 * @author aluno
 */
public class Dados {
    // Database
    private static String username, password;

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        Dados.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        Dados.password = password;
    }
}
